#include <stdio.h>
#include <string.h>

void init(void);

int main(void)
{
	init();

    char word[260];
    char giveflag[6];

    strcpy(giveflag, "No.");

    printf("What's your favorite word? ");
    scanf("%s", word);
    if ( !strcmp(giveflag, "Yesss!") ) {
        #ifdef PROD
            puts("Flag is: ctf_isp{str1ng_0verflow}");
        #else
            puts("Flag is ctf_isp{****************}");
        #endif
    } else {
        puts("Okay, I don't give you flag");
    }

    return 0;
}

void init(void)
{
    setvbuf(stdin, 0, 2, 0);
    setvbuf(stdout, 0, 2, 0);
    setvbuf(stderr, 0, 2, 0);
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define STDIN 0

char padding[4096];
char buf2[256];

void init(void);
void play(void);
void vuln(void);
void win(void);

int main(void)
{
	init();

	play();

	return 0;
}

void init(void)
{
	setvbuf(stdin, 0, 2, 0);
	setvbuf(stdout, 0, 2, 0);
	setvbuf(stderr, 0, 2, 0);
}

void play(void)
{
	int j = 0;
	vuln();
}

void vuln(void)
{
	char buf1[8];

	puts("Let's overflow saved rbp! reading 16 bytes:");
	read(STDIN, buf1, 16);

	puts("Let's prepare some handy buffer! reading 8 bytes:");
	read(STDIN, buf2, 8);

	puts("GL&HF!");
}

void win(void)
{
	system("/bin/sh");
}

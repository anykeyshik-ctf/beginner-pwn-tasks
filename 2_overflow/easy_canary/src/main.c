#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void init(void);
void vuln(void);
void win(void);

int main(void)
{
    init();
    vuln();

    return 0;
}

void init(void)
{
    setvbuf(stdin, 0, 2, 0);
    setvbuf(stdout, 0, 2, 0);
    setvbuf(stderr, 0, 2, 0);
}

void vuln(void)
{
    char format[40];
    char buf[100];

    scanf("%40s", format);
    printf(format);

    write(1, "\nwrite again!\n", 14);
    read(0, buf, 256);
}

void win(void)
{
    system("/bin/sh");
}

#include <stdio.h>
#include <string.h>

void init(void);

int main(void)
{
	init();

    int count;
	int i;
    char giveflag[6];
    int storage[5];

    strcpy(giveflag, "No.");

    printf("How many number do you want to store (max. 5)? ");
    scanf("%d", &count);

    for (i = 0; i < count; i++) {
        printf("Enter number #%d: ", i + 1);
        scanf("%d", &storage[i]);
    }

    printf("Here is your numbers: ");
    for (i = 0; i < count; i++) {
        if (i > 0 && i < count) {
            printf(", ");
        }
        printf("%d", storage[i]);
    }
    puts("\n");

    printf("Do you want the flag?\n%s\n", giveflag);
    if ( !strcmp(giveflag, "YEAH") ) {
        #ifdef PROD
            puts("Flag is: ctf_isp{numb3r_0verfl0w}");
        #else
            puts("Flag is: ctf_isp{***************}");
        #endif
    } else {
        puts("Okay, I don't give you flag");
    }

    return 0;
}

void init(void)
{
    setvbuf(stdin, 0, 2, 0);
    setvbuf(stdout, 0, 2, 0);
    setvbuf(stderr, 0, 2, 0);
}

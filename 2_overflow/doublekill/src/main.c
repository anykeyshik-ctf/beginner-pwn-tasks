#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char password[256];

void init(void);
void win(void);

int main(void)
{
	init();

    char *password_ptr = password;
    char username[40];

    printf("DEBUG> username: %p password: %p\n", username, password);

    printf("Enter username: ");
    read(0, username, 48);

    printf("Enter password: ");
    read(0, password_ptr, 8);

    return 0;
}

void init(void)
{
    setvbuf(stdin, 0, 2, 0);
    setvbuf(stdout, 0, 2, 0);
    setvbuf(stderr, 0, 2, 0);
}

void win(void)
{
    system("/bin/sh");
}


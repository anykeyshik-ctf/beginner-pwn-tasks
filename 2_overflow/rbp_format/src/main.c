#include <stdio.h>
#include <stdlib.h>

void init(void);
void play(void);
void vuln(void);
void win(void);

char fmt[100];

int main(void)
{
    init();

    while (1) {
		play();
	}

    return 0;
}

void init(void)
{
    setvbuf(stdin, 0, 2, 0);
    setvbuf(stdout, 0, 2, 0);
    setvbuf(stderr, 0, 2, 0);
}

void play(void)
{
    vuln();
}

void vuln(void)
{
    fgets(fmt, 100, stdin);
    printf(fmt);
}

void win(void)
{
    system("/bin/sh");
}

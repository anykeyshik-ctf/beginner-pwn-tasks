#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void init(void);

int main(void)
{
    char *string_ptr;
    char buf[8];

    string_ptr = "NO WIN";

    puts("Let's overwrite a pointer to C string!");
    fflush(stdout);
    read(0, buf, 32);

    if ( !strcmp(string_ptr, "WIN") ) {
        system("/bin/sh");
    }

    return 0;
}

void init(void)
{
	setvbuf(stdin, 0, 2, 0);
	setvbuf(stdout, 0, 2, 0);
	setvbuf(stderr, 0, 2, 0);
}

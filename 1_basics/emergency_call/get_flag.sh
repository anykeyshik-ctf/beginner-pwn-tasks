#!/usr/bin/env bash

grep "gef" ~/.gdbinit 2>/dev/null 1>/dev/null
if [[ $? -eq 0 ]]; then
	sed -i "s/^#gef config context.enable 0/gef config context.enable 0/" exploit.gdb
else
	sed -i "s/^gef config context.enable 0/#gef config context.enable 0/" exploit.gdb
fi

gdb -q --batch --command=./exploit.gdb ./call_me

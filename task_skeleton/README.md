# Docker task skeleton

## Important
Please provide **working** exploit for your challenge and don't forget to add it to the `docker-compose` file

### Description

`data` dir

This dir is for challenge and its config on the server. All challenges run through `xinetd`

`files` dir

For files which will be given to players

`src` dir

For challenge source

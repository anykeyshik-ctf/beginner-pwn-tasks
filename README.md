# PWN tasks for beginners 

This repo contains some simple but interesting tasks for newbies in binary analysis
All tasks provides with writeup and deploy config

### List of topics:

1. Generaly usage of debugger [tasks](./1_basics)
2. Simple buffer overflow and canary bypass with format string. Contains hard task named `rbp_format` [tasks](./2_overflow)
